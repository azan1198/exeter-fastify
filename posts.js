var students = {
  data: [],
};

module.exports = async function routes(fastify, option) {
  fastify.get("/report", () => {
    return students;
  });

  fastify.post("/add", async function (request, reply) {
    const {
      studentName,
      studentID,
      subject1,
      subject2,
      subject3,
      subject4,
      subject5,
    } = request.body;

    if (students.data.length > 0) {
      ifDuplicate = students.data.findIndex(
        (obj) => obj.studentID == studentID
      );
      //Check if there is no duplicate
      if (ifDuplicate >= 0) {
        return `Student ID ${studentID} already exists. Assign new StudentID`;
      }
    }

    const newdata = {
      studentName: studentName,
      studentID: studentID,
      subject1: subject1,
      subject2: subject2,
      subject3: subject3,
      subject4: subject4,
      subject5: subject5,
    };

    students.data.push(newdata);
    return newdata;
  });

  fastify.delete("/delete/:id", async function (request, reply) {
    let { id } = request.params;

    //Check if student Id exists
    ifDuplicate = students.data.findIndex((obj) => obj.studentID == id);
    if (ifDuplicate < 0) {
      return `Student ID ${id} does not exists.`;
    }

    students.data = students.data.filter(
      (deletedata) => deletedata.studentID != id
    );
    reply.send({ message: `StudentID ${id} is deleted` });
  });

  fastify.put("/update/:id", async function (request, reply) {
    let { id } = request.params;

    //Check if student Id exists
    ifAvailable = students.data.findIndex((obj) => obj.studentID == id);
    if (ifAvailable < 0) {
      return `Student ID ${id} does not exist.`;
    }

    const { subject1, subject2, subject3, subject4, subject5 } = request.body;

    objIndex = students.data.findIndex((obj) => obj.studentID == id);
    students.data[objIndex].subject1 = subject1;
    students.data[objIndex].subject2 = subject2;
    students.data[objIndex].subject3 = subject3;
    students.data[objIndex].subject4 = subject4;
    students.data[objIndex].subject5 = subject5;
    reply.send({ message: `Student ID ${id} is updated` });
  });
};
