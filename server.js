const fastify = require("fastify");
const app = fastify();

const postRoutes = require("./posts");

app.register(postRoutes);
app.listen(4000, () => console.log("server up at http://localhost:4000"));
